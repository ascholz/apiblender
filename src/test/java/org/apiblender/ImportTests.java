package org.apiblender;

import static org.apiblender.YamlTools.importReferences;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;


public class ImportTests {
    private static final String[] DEFINITIONS = new String[] {
            "definitions:",
            "  a:",
            "    key: value",
            "  b:",
            "    key: value",
            
    };
    
    
    private static final String[] SINGLE_REF = new String[] {
            "a:",
            "  $ref: '#/definitions/a'",
            "b:"
    };
    
    @Test
    public void testSingleRef() {
        String[] expected = new String[] {
                "a:",
                "  $ref: '#/definitions/a'",
                "b:",
                "definitions:",
                "  a:",
                "    key: value",
                
        };
        
        assertArrayEquals(expected, importReferences(new Document(SINGLE_REF), new Document(DEFINITIONS)).linesArray());
    }
    
    
    
    private static final String[] SINGLE_REF_EXISTING_BASE = new String[] {
            "definitions:",
            "a:",
            "  $ref: '#/definitions/a'",
            "b:"
    };
    
    @Test
    public void testSingleRefExistingBase() {
        String[] expected = new String[] {
                "definitions:",
                "  a:",
                "    key: value",
                "a:",
                "  $ref: '#/definitions/a'",
                "b:",
        };
        
        assertArrayEquals(expected, importReferences(new Document(SINGLE_REF_EXISTING_BASE), new Document(DEFINITIONS)).linesArray());
    }
    
    
    
    private static final String[] SINGLE_REF_EXISTING = new String[] {
            "a:",
            "  $ref: '#/definitions/a'",
            "b:",
            "definitions:",
            "  a:",
            "    a: b",
    };
    
    @Test
    public void testSingleRefExisting() {
        String[] expected = new String[] {
                "a:",
                "  $ref: '#/definitions/a'",
                "b:",
                "definitions:",
                "  a:",
                "    a: b",
        };
        
        assertArrayEquals(expected, importReferences(new Document(SINGLE_REF_EXISTING), new Document(DEFINITIONS)).linesArray());
    }
}
