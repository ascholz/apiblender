package org.apiblender;

import static org.apiblender.YamlTools.patch;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class PatchReplaceTests {
    private static final String[] DOC = new String[] {
            "a:",
            "  a:",
            "    k: v",
            "  b:",
            "    a:",
            "      k: v",
            "    b:",
            "     k: v",
            "b:",
            "  k: v"
    };
    
    
    
    private static final String[] REPLACE_ROOT = new String[] {
            "1:",
            "  op: replace",
            "  path: /",
            "  content:",
            "    a:",
            "      key: value"
    };
    
    @Test
    public void testReplaceRoot() {
        String[] expected = new String[] {
                "a:",
                "  key: value"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REPLACE_ROOT)).linesArray());
    }
    
    
    
    private static final String[] REPLACE_LEVEL1 = new String[] {
            "1:",
            "  op: replace",
            "  path: /a",
            "  content:",
            "      key: value"
    };
    
    @Test
    public void testReplaceLevel1() {
        String[] expected = new String[] {
                "a:",
                "  key: value",
                "b:",
                "  k: v"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REPLACE_LEVEL1)).linesArray());
    }
    
    
    
    private static final String[] REPLACE_LEVEL1_EMPTY = new String[] {
            "1:",
            "  op: replace",
            "  path: /a",
            "  content:",
    };
    
    @Test
    public void testReplaceLevel1Empty() {
        String[] expected = new String[] {
                "a:",
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REPLACE_LEVEL1_EMPTY)).linesArray());
    }
    
    
    
    private static final String[] REPLACE_SCALAR_NODE = new String[] {
            "1:",
            "  op: replace",
            "  path: /a/a/k",
            "  content:",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testReplaceScalarNode() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REPLACE_SCALAR_NODE)).linesArray());
    }
    
    
    
    private static final String[] REMOVE_NON_EXISTING = new String[] {
            "1:",
            "  op: replace",
            "  path: /c",
            "  content:",
    };
    
    @Test
    public void testReplaceNonExisting() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
                "c:"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REMOVE_NON_EXISTING)).linesArray());
    }
}
