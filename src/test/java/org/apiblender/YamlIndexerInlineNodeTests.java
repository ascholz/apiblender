package org.apiblender;

import static org.apiblender.YamlIndexer.index;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class YamlIndexerInlineNodeTests {
    private static final String[] FIRST_LEVEL_YAML = new String[] {
            "a: avalue",
            "b: bvalue"
    };
    
    @Test
    public void testFindWithFirstLevelNodes() {
        String[] expectedA = new String[] {
                "a: avalue",
        };
        String[] expectedB = new String[] {
                "b: bvalue"
        };
        
        YamlIndex index = index(new Document(FIRST_LEVEL_YAML));
        
        assertArrayEquals(expectedA, index.find(YamlPath.from("a")).get().linesArray());
        assertArrayEquals(expectedB, index.find(YamlPath.from("b")).get().linesArray());
    }
    
    
    
    private static final String[] DEEP_YAML = new String[] {
            "a:",
            "  key: value",
            "  suba:",
            "    key: value",
            "b: bvalue"
    };
    
    @Test
    public void testFindWithDeepNodes() {
        String[] expectedAKey = new String[] {
                "  key: value",
        };
        String[] expectedSuba = new String[] {
                "  suba:",
                "    key: value",
        };
        String[] expectedSubaKey = new String[] {
                "    key: value",
        };
        
        YamlIndex index = index(new Document(DEEP_YAML));
        
        assertArrayEquals(expectedAKey, index.find(YamlPath.from("a", "key")).get().linesArray());
        assertArrayEquals(expectedSuba, index.find(YamlPath.from("a", "suba")).get().linesArray());
        assertArrayEquals(expectedSubaKey, index.find(YamlPath.from("a", "suba", "key")).get().linesArray());
    }
}
