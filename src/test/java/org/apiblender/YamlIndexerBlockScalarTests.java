package org.apiblender;

import static org.apiblender.YamlIndexer.index;
import static org.junit.Assert.*;

import org.junit.Test;

public class YamlIndexerBlockScalarTests {
    private static final String[] BLOCK_INDICATOR_YAML = new String[] {
            "a: |",
            "  I am a value",
            "b: >",
            "  I am a value",
            "c: >-",
            "  I am a value",
            "d: >+",
            "  I am a value",
            "e: |+",
            "  I am a value",
            "f: >1+",
            "  I am a value",
    };
    
    @Test
    public void testFindWithBlockIndicators() {
        YamlIndex index = index(new Document(BLOCK_INDICATOR_YAML));
        
        for (String key : "a,b,c,d,e,f".split(",")) {
            assertEquals(2, index.find(YamlPath.from(key)).get().linesArray().length);
        }
    }
    
    
    
    private static final String[] BLOCK_WITH_INDENTS = new String[] {
            "a: |",
            "  I am a value",
            "   I am a value",
            "   I am a value",
            "  I am a value",
            "b:",
            "  a: I am a value",
    };
    
    @Test
    public void testFindWithBlockWithIndents() {
        String[] expected = new String[] {
                "a: |",
                "  I am a value",
                "   I am a value",
                "   I am a value",
                "  I am a value"
        };
        
        YamlIndex index = index(new Document(BLOCK_WITH_INDENTS));
        
        assertArrayEquals(expected, index.find(YamlPath.from("a")).get().linesArray());
    }
}
