package org.apiblender;

import static org.apiblender.PatchIndexer.index;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class PatchIndexerTests {
    private static final String[] MISSING_OP = new String[] {
            "1:",
            "  path: /",
            "  content:",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testIndexThrowsOnMissingOp() {
        index(new Document(MISSING_OP));
    }
    
    
    
    private static final String[] INVALID_OP = new String[] {
            "1:",
            "  op:",
            "  path: /",
            "  content:",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testIndexThrowsOnInvalidOp() {
        index(new Document(INVALID_OP));
    }
    
    
    
    private static final String[] UNKNOWN_OP = new String[] {
            "1:",
            "  op: doesNotExist",
            "  path: /",
            "  content:",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testIndexThrowsOnUnknownOp() {
        index(new Document(UNKNOWN_OP));
    }
    
    
    
    private static final String[] MISSING_PATH = new String[] {
            "1:",
            "  op: append",
            "  content:",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testIndexThrowsOnMissingPath() {
        index(new Document(MISSING_PATH));
    }
    
    
    
    private static final String[] INVALID_PATH = new String[] {
            "1:",
            "  op: append",
            "  path:",
            "  content:",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testIndexThrowsOnInvalidPath() {
        index(new Document(INVALID_PATH));
    }
    
    
    
    private static final String[] MISSING_CONTENT = new String[] {
            "1:",
            "  op: append",
            "  path: /",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testIndexThrowsOnMissingContent() {
        index(new Document(MISSING_CONTENT));
    }
    
    
    
    private static final String[] MULTIPLE_PATCHES = new String[] {
            "1:",
            "  op: append",
            "  path: /",
            "  content:",
            "    key: value",
            "",
            "2:",
            "  op: append",
            "  path: /a",
            "  content:",
            "    key: value",
    };
    
    @Test
    public void testIndexWithMultiplePatches() {
        assertEquals(2, index(new Document(MULTIPLE_PATCHES)).patches().size());
    }
}
