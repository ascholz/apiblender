package org.apiblender;

import static org.apiblender.YamlTools.patch;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class PatchAppendTests {
    private static final String[] DOC = new String[] {
            "a:",
            "  a:",
            "    k: v",
            "  b:",
            "    a:",
            "      k: v",
            "    b:",
            "     k: v",
            "b:",
            "  k: v"
    };
    
    private static final String[] APPEND_ROOT = new String[] {
            "1:",
            "  op: append",
            "  path: /",
            "  content:",
            "    key: value",
    };
    
    @Test
    public void testAppendAtRoot() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
                "key: value"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_ROOT)).linesArray());
    }
    
    
    
    private static final String[] APPEND_LEVEL1 = new String[] {
            "1:",
            "  op: append",
            "  path: /a",
            "  content:",
            "    key: value",
    };
    
    @Test
    public void testAppendLevel1() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "  key: value",
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_LEVEL1)).linesArray());
    }
    
    
    
    private static final String[] APPEND_LEVEL2 = new String[] {
            "1:",
            "  op: append",
            "  path: /a/a",
            "  content:",
            "    key: value",
    };
    
    @Test
    public void testAppendLevel2() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "    key: value",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_LEVEL2)).linesArray());
    }
    
    
    
    private static final String[] APPEND_VALUE_NODE = new String[] {
            "1:",
            "  op: append",
            "  path: /a/a/k",
            "  content:",
            "    key: value",
    };
    
    @Test(expected = IllegalStateException.class)
    public void testAppendAtValueNode() {
        patch(new Document(DOC), new Document(APPEND_VALUE_NODE)).linesArray();
    }
    
    
    
    private static final String[] APPEND_MISMATCHING_INDENT = new String[] {
            "1:",
            "  op: append",
            "  path: /a/a",
            "  content:",
            "      key: value",
            "      key2: value2",
    };
    
    @Test
    public void testAppendMismatchingIndent() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "    key: value",
                "    key2: value2",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_MISMATCHING_INDENT)).linesArray());
    }
    
    
    
    private static final String[] APPEND_EMPTY = new String[] {
            "1:",
            "  op: append",
            "  path: /a/a",
            "  content:",
    };
    
    @Test
    public void testAppendEmpty() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_EMPTY)).linesArray());
    }
    
    
    
    private static final String[] APPEND_EMPTY_NON_EXISTING = new String[] {
            "1:",
            "  op: append",
            "  path: /c",
            "  content:",
    };
    
    @Test
    public void testAppendEmptyAtNonExisting() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
                "c:"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_EMPTY_NON_EXISTING)).linesArray());
    }
    
    
    
    private static final String[] APPEND_EMPTY_NON_EXISTING2 = new String[] {
            "1:",
            "  op: append",
            "  path: /a/c",
            "  content:",
    };
    
    @Test
    public void testAppendEmptyAtNonExisting2() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "  c:",
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_EMPTY_NON_EXISTING2)).linesArray());
    }
    
    
    
    private static final String[] APPEND_NON_EXISTING = new String[] {
            "1:",
            "  op: append",
            "  path: /c",
            "  content:",
            "    key: value"
    };
    
    @Test
    public void testAppendNonExisting() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "    k: v",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v",
                "c:",
                "  key: value"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(APPEND_NON_EXISTING)).linesArray());
    }
}
