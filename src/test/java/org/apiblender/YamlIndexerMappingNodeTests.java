package org.apiblender;

import static org.apiblender.YamlIndexer.index;
import static org.apiblender.YamlPath.root;
import static org.junit.Assert.*;

import org.junit.Test;

public class YamlIndexerMappingNodeTests {
    private static final String[] TWO_NODE_YAML = new String[] {
            "a:",
            "  suba:",
            "b:",
            "  subb:"
    };
    
    @Test
    public void testFindWithRootReturnsWholeDocument() {
        YamlIndex index = index(new Document(TWO_NODE_YAML));
        assertArrayEquals(TWO_NODE_YAML, index.find(root()).get().linesArray());
    }
    
    @Test
    public void testFindWithFirstLevelNodes() {
        String[] expectedA = new String[] {
                "a:",
                "  suba:"
        };
        String[] expectedB = new String[] {
                "b:",
                "  subb:"
        };
        
        YamlIndex index = index(new Document(TWO_NODE_YAML));
        
        assertArrayEquals(expectedA, index.find(YamlPath.from("a")).get().linesArray());
        assertArrayEquals(expectedB, index.find(YamlPath.from("b")).get().linesArray());
    }
    
    
    
    private static final String[] TWO_SUBNODE_YAML = new String[] {
            "a:",
            "  suba:",
            "  suba2:",
            "b:",
            "  subb:"
    };
    
    @Test
    public void testFindWithMultipleSubNodes() {
        String[] expectedA = new String[] {
                "a:",
                "  suba:",
                "  suba2:"
        };
        String[] expectedB = new String[] {
                "b:",
                "  subb:"
        };
        
        YamlIndex index = index(new Document(TWO_SUBNODE_YAML));
        
        assertArrayEquals(expectedA, index.find(YamlPath.from("a")).get().linesArray());
        assertArrayEquals(expectedB, index.find(YamlPath.from("b")).get().linesArray());
    }
    
    
    
    private static final String[] VARIABLE_INDENT_YAML = new String[] {
            "a:",
            " suba1:",
            " suba2:",
            "b:",
            "  subb1:",
            "  subb2:"
            
    };
    
    @Test
    public void testFindWithVariableIndent() {
        String[] expectedA = new String[] {
                "a:",
                " suba1:",
                " suba2:"
        };
        String[] expectedB = new String[] {
                "b:",
                "  subb1:",
                "  subb2:"
        };
        
        YamlIndex index = index(new Document(VARIABLE_INDENT_YAML));
        
        assertArrayEquals(expectedA, index.find(YamlPath.from("a")).get().linesArray());
        assertArrayEquals(expectedB, index.find(YamlPath.from("b")).get().linesArray());
    }
    
    
    private static final String[] DEEP_YAML = new String[] {
            "a:",
            "  sa:",
            "    ssa:",
            "      sssa:",
            "        key: value"
            
    };
    
    @Test
    public void testFindWithDeepYaml() {
        String[] expectedA = new String[] {
                "      sssa:",
                "        key: value"
        };
        
        YamlIndex index = index(new Document(DEEP_YAML));
        
        assertArrayEquals(expectedA, index.find(YamlPath.from("a", "sa", "ssa", "sssa")).get().linesArray());
    }
    
    
    
    private static final String[] WITH_SLASHES = new String[] {
            "a:",
            "  /path:",
            "    key: value",
            
    };
    
    @Test
    public void testFindWithSlashes() {
        String[] expected = new String[] {
                "  /path:",
                "    key: value",
        };
        
        YamlIndex index = index(new Document(WITH_SLASHES));
        
        assertArrayEquals(expected, index.find(YamlPath.from("a", "/path")).get().linesArray());
    }
    
    
    
    private static final String[] WITH_EMPTY_LINE = new String[] {
            "a:",
            "  b:",
            "      ",
            "",
            "b:",
    };
    
    @Test
    public void testFindWithBlockWithIndents() {
        String[] expected = new String[] {
                "a:",
                "  b:"
        };
        
        YamlIndex index = index(new Document(WITH_EMPTY_LINE));
        
        assertArrayEquals(expected, index.find(YamlPath.from("a")).get().linesArray());
    }
}
