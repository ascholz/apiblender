package org.apiblender;

import static org.apiblender.YamlTools.patch;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class PatchRemoveTests {
    private static final String[] DOC = new String[] {
            "a:",
            "  a:",
            "    k: v",
            "  b:",
            "    a:",
            "      k: v",
            "    b:",
            "     k: v",
            "b:",
            "  k: v"
    };
    
    
    
    private static final String[] REMOVE_ROOT = new String[] {
            "1:",
            "  op: remove",
            "  path: /",
            "  content:",
    };
    
    @Test
    public void testRemoveRoot() {
        String[] expected = new String[] {
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REMOVE_ROOT)).linesArray());
    }
    
    
    
    private static final String[] REMOVE_LEVEL1 = new String[] {
            "1:",
            "  op: remove",
            "  path: /a",
            "  content:",
    };
    
    @Test
    public void testRemoveLevel1() {
        String[] expected = new String[] {
                "b:",
                "  k: v",
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REMOVE_LEVEL1)).linesArray());
    }
    
    
    
    private static final String[] REMOVE_SCALAR_NODE = new String[] {
            "1:",
            "  op: remove",
            "  path: /a/a/k",
            "  content:",
    };
    
    @Test
    public void testRemoveScalarNode() {
        String[] expected = new String[] {
                "a:",
                "  a:",
                "  b:",
                "    a:",
                "      k: v",
                "    b:",
                "     k: v",
                "b:",
                "  k: v"
        };
        
        assertArrayEquals(expected, patch(new Document(DOC), new Document(REMOVE_SCALAR_NODE)).linesArray());
    }
    
    
    
    private static final String[] REMOVE_NON_EXISTING = new String[] {
            "1:",
            "  op: remove",
            "  path: /c",
            "  content:",
    };
    
    @Test
    public void testRemoveNonExisting() {
        assertArrayEquals(DOC, patch(new Document(DOC), new Document(REMOVE_NON_EXISTING)).linesArray());
    }
}
