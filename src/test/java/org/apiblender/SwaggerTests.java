package org.apiblender;

import static org.apiblender.YamlIndexer.index;
import static org.junit.Assert.assertArrayEquals;

import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;


public class SwaggerTests {
    @Test
    public void testFindWithBlockWithIndents() {
        String[] expected = new String[] {
                "  version: '3.0'"
        };
        
        assertArrayEquals(expected, index(swaggerDoc).find(YamlPath.from("info", "version")).get().linesArray());
    }
    
    
    @Test
    public void testPatch() throws IOException {
        Document patchDoc = new Document(Paths.get("src", "test", "resources", "example_swagger_patch.yaml"));
        
        Document patchedDoc = YamlTools.patch(swaggerDoc, patchDoc);
        
        System.out.println(patchedDoc.asString());
    }
    
    
    @Before
    public void indexSwagger() throws IOException {
        swaggerDoc = new Document(Paths.get("src", "test", "resources", "example_swagger.yaml"));
    }
    
    private Document swaggerDoc;
}
