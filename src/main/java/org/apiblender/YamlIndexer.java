package org.apiblender;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class YamlIndexer {
    // FIXME: currently no real support for lists, "-" are just stripped
    private static final String nodeNameRegex = "\\s*([^:\\- \\t][^:]*):";
    private static final Pattern mappingNodePattern = Pattern.compile(nodeNameRegex + "(\\s*&\\w+)?\\s*");
    private static final Pattern inlineNodePattern = Pattern.compile(nodeNameRegex + "\\s+[^:|>]+\\s*");
    private static final Pattern blockScalarNodePattern = Pattern.compile(nodeNameRegex + "\\s+[\\|>]\\d*[+\\-]?\\s*");
    
    public static YamlIndex index(Document doc) {
        YamlNode root = new YamlNode(doc);
        
        indexMappingNode(doc, 0, root);
        
        return new YamlIndex(root);
    }
    
    public static int indexMappingNode(Document doc, int startLine, YamlNode parent) {
        int blockIndent = -1;
        
        for (int currentLine = startLine; currentLine < doc.lines().size(); currentLine++) {
            String line = doc.lines().get(currentLine);
            
            if (line.trim().isEmpty()) {
                continue;
            }
            int lineIndent = getIndent(line);
            
            /* end of block, retain current line for parsing in parent */
            if (lineIndent <= parent.getIndent()) {
                return currentLine;
            }
            
            /* check for first child */
            if (blockIndent == -1) {
                blockIndent = lineIndent;
                parent.setChildIndent(blockIndent);
            }
            
            if (lineIndent != blockIndent) {
                throw new IllegalStateException("inconsistent indent in line " + currentLine + ": " + line);
            }
            
            Matcher mappingNode = mappingNodePattern.matcher(line);
            Matcher inlineNode = inlineNodePattern.matcher(line);
            Matcher blockScalar = blockScalarNodePattern.matcher(line);
            if (mappingNode.matches()) {
                YamlNode child = new YamlNode(parent, doc, mappingNode.group(1), blockIndent, currentLine);
                currentLine = indexMappingNode(doc, currentLine + 1, child);
                currentLine--;
                
                /* trim empty lines */
                int endLine = currentLine;
                while (endLine > startLine && doc.lines().get(endLine).trim().isEmpty()) {
                    endLine--;
                }
                
                child.setIsMappingNode(true);
                child.setEndLine(endLine);
            }
            else if (blockScalar.matches()) {
                YamlNode child = new YamlNode(parent, doc, blockScalar.group(1), blockIndent, currentLine);
                currentLine = indexBlockScalarNode(doc, currentLine + 1, child.getIndent());
                currentLine--;
                child.setEndLine(currentLine);
            }
            else if (inlineNode.matches()) {
                new YamlNode(parent, doc, inlineNode.group(1), blockIndent, currentLine).setEndLine(currentLine);
            }
            else if (line.trim().startsWith("-")) {
                currentLine = indexBlockScalarNode(doc, currentLine + 1, blockIndent);
                currentLine--;
            }
        }
        
        return doc.lines().size();
    }
    
    
    public static int indexBlockScalarNode(Document doc, int startLine, int parentIndent) {
        for (int currentLine = startLine; currentLine < doc.lines().size(); currentLine++) {
            String line = doc.lines().get(currentLine);
            
            if (line.trim().isEmpty()) {
                continue;
            }
            int lineIndent = getIndent(line);
            
            /* end of block, retain current line for parsing in parent */
            if (lineIndent <= parentIndent) {
                return currentLine;
            }
        }
        
        return doc.lines().size();
    }
    
    
    public static int getIndent(String line) {
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) != ' ') {
                return i;
            }
        }
        
        return line.length();
    }
}
