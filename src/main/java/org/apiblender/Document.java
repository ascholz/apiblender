package org.apiblender;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


public class Document {

    public Document(Path path) throws IOException {
        this(Files.readAllLines(path, StandardCharsets.US_ASCII));
    }
    
    public Document(String ... lines) {
        m_lines.addAll(Arrays.asList(Objects.requireNonNull(lines, "lines must not be null")));
    }
    
    public Document(List<String> lines) {
        m_lines.addAll(Objects.requireNonNull(lines, "lines must not be null"));
    }
    
    
    public List<String> lines() {
        return m_linesView;
    }
    
    
    public String[] linesArray() {
        return lines().toArray(new String[0]);
    }
    
    
    public String asString() {
        return String.join("\n", m_lines);
    }

    
    private final List<String> m_lines = new ArrayList<>();
    private final List<String> m_linesView = Collections.unmodifiableList(m_lines);
}
