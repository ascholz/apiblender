package org.apiblender;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apiblender.CmdLineArgs.CommandImport;
import org.apiblender.CmdLineArgs.CommandPatch;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;


public class ApiBlender {

    public static void main(String[] args) throws IOException {
        CommandPatch cmdPatch = new CommandPatch();
        CommandImport cmdImport = new CommandImport();
        
        JCommander jc = JCommander.newBuilder()
            .addCommand("patch", cmdPatch)
            .addCommand("import", cmdImport)
            .build();
            
        try {
            jc.parse(args);
            
            Document result;
            switch (jc.getParsedCommand()) {
            case "patch":
                result = YamlTools.patch(new Document(Paths.get(cmdPatch.getInput())), 
                        new Document(Paths.get(cmdPatch.getPatch())));
                
                if (cmdPatch.getOutput() == null) {
                    System.out.println(result.asString());
                }
                else {
                    Files.write(Paths.get(cmdPatch.getOutput()), result.lines());
                }
                break;
            case "import":
                result = YamlTools.importReferences(new Document(Paths.get(cmdImport.getInput())), 
                        new Document(Paths.get(cmdImport.getDefinitions())));
                
                if (cmdImport.getOutput() == null) {
                    System.out.println(result.asString());
                }
                else {
                    Files.write(Paths.get(cmdImport.getOutput()), result.lines());
                }
                break;
            default:
                jc.usage();
                break;
            }
        }
        catch (ParameterException e) {
            jc.usage();
        }
    }
}
