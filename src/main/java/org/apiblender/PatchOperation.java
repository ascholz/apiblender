package org.apiblender;


public enum PatchOperation {
    APPEND, REPLACE, REMOVE
}
