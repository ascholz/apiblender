package org.apiblender;

import java.util.Optional;

public class YamlTools {
    public static Document patch(Document original, Document patches) {
        Document patched = original;
        
        for (Patch patch : PatchIndexer.index(patches).patches()) {
            patched = patch(patched, patch);
        }
        
        return patched;
    }

    public static Document patch(Document original, Patch patch) {
        return patch.applyTo(original);
    }
    
    public static Document importReferences(Document original, Document imports) {
        Document result = original;
        
        YamlIndex importsIndex = YamlIndexer.index(imports);
        
        boolean modified = true;
        
        while (modified) {
            modified = false;
            
            for (String pathString : RefScanner.findRefs(result)) {
                YamlPath path = YamlPath.parse(pathString);
                YamlIndex docIndex = YamlIndexer.index(result);
                
                Optional<YamlNode> docMatch = docIndex.find(path);
                Optional<YamlNode> importsMatch = importsIndex.find(path);
                
                if (!docMatch.isPresent() && importsMatch.isPresent()) {
                    Patch importPatch = new Patch(PatchOperation.APPEND, path, importsMatch.get());
                    
                    result = patch(result, importPatch);
                    
                    modified = true;
                    
                    break;
                }
                else if (!docMatch.isPresent()) {
                    System.err.println("no defintion found for ref: " + pathString);
                }
            }
        }
        
        return result;
    }
}
