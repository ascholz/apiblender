package org.apiblender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class YamlPath {
    private static final Pattern segmentPattern = Pattern.compile("([a-zA-Z_0-9\\-]+|'[^']*')/?");
    
    public static YamlPath from(String ... segments) {
        return new YamlPath(Arrays.asList(segments));
    }
    
    public static YamlPath from(Collection<String> segments) {
        return new YamlPath(Objects.requireNonNull(segments, "segments must not be null"));
    }
    
    public static YamlPath parse(String path) {
        Collection<String> pathSegments = new ArrayList<>();
        
        Matcher segmentMatcher = segmentPattern.matcher(path);
        while (segmentMatcher.find()) {
            pathSegments.add(segmentMatcher.group(1));
        }
        
        return YamlPath.from(pathSegments);
    }
    
    public static YamlPath root() {
        return from();
    }
    
    
    public boolean isRoot() {
        return m_segments.isEmpty();
    }
    
    
    public List<String> segments() {
        return m_segmentsView;
    }
    
    
    private YamlPath(Collection<String> segments) {
        m_segments.addAll(segments);
    }
    
    private final List<String> m_segments = new ArrayList<>();
    private final List<String> m_segmentsView = Collections.unmodifiableList(m_segments);
}
