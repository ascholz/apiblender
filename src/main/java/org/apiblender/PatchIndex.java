package org.apiblender;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class PatchIndex {
    public PatchIndex(Collection<Patch> patches) {
        m_patches.addAll(patches);
    }

    
    public Collection<Patch> patches() {
        return m_patchesView;
    }
    
    
    private final Collection<Patch> m_patches = new ArrayList<>();
    private final Collection<Patch> m_patchesView = Collections.unmodifiableCollection(m_patches);
}
