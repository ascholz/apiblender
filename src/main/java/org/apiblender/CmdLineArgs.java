package org.apiblender;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

public class CmdLineArgs {
    @Parameters(commandDescription = "Apply patch")
    public static class CommandPatch {

        public String getInput() {
            return m_input;
        }
        
        public String getPatch() {
            return m_patch;
        }
        
        public String getOutput() {
            return m_output;
        }

        @Parameter(names={"--input", "-i"}, description="input document", required=true)
        private String m_input;

        @Parameter(names={"--patch", "-p"}, description="patch to apply to input", required=true)
        private String m_patch;
        
        @Parameter(names={"--output", "-o"}, description="output file", required=false)
        private String m_output;
    }
    
    
    @Parameters(commandDescription = "Import references")
    public static class CommandImport {

        public String getInput() {
            return m_input;
        }
        
        public String getDefinitions() {
            return m_definitions;
        }
        
        public String getOutput() {
            return m_output;
        }

        @Parameter(names={"--input", "-i"}, description="input document with references", required=true)
        private String m_input;

        @Parameter(names={"--defs", "-d"}, description="document containing definitions", required=true)
        private String m_definitions;
        
        @Parameter(names={"--output", "-o"}, description="output file", required=false)
        private String m_output;
    }
}
