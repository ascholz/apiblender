package org.apiblender;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RefScanner {

    private static final Pattern pattern = Pattern.compile("\\s*\\$ref:[ ]+'#([^']+)'\\s*");
    
    
    public static Set<String> findRefs(Document doc) {
        Set<String> matches = new HashSet<>();
        
        for (String line : doc.lines()) {
            Matcher m = pattern.matcher(line);
            if (m.matches()) {
                matches.add(m.group(1));
            }
        }
            
        return matches;
    }
}
