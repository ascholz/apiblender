package org.apiblender;

import java.io.PrintWriter;
import java.util.Optional;

public class YamlIndex {

    public YamlIndex(YamlNode root) {
        m_root = root;
    }

    
    public Optional<YamlNode> find(YamlPath path) {
        if (path.isRoot()) {
            return Optional.of(m_root);
        }
        
        YamlNode parent = m_root;
        for (String segmentName : path.segments()) {
            parent = parent.getChild(segmentName);
            
            if (null == parent) {
                return Optional.empty();
            }
        }
        
        return Optional.of(parent);
    }
    
    
    public YamlNode root() {
        return m_root;
    }
    
    
    public void dump(PrintWriter writer) {
        m_root.dump(writer);
    }


    private final YamlNode m_root;
}
