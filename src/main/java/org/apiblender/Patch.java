package org.apiblender;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Patch {
    
    public Patch(PatchOperation operation, YamlPath path, YamlNode content) {
        m_operation = Objects.requireNonNull(operation, "operation must not be null");
        m_path = Objects.requireNonNull(path, "path must not be null");
        m_content = Objects.requireNonNull(content, "content must not be null");
    }


    public PatchOperation getOperation() {
        return m_operation;
    }
    
    
    public YamlPath getPath() {
        return m_path;
    }
    
    
    public YamlNode getContent() {
        return m_content;
    }
    
    
    public Document applyTo(Document doc) {
        switch (m_operation) {
        case APPEND:
            return append(doc);
        case REPLACE:
            return replace(doc);
        case REMOVE:
            return remove(doc);
        default:
            throw new UnsupportedOperationException();
        }
    }

    
    private Document replace(Document doc) {
        YamlIndex docIndex = YamlIndexer.index(doc);
        
        Optional<YamlNode> matchOpt = docIndex.find(getPath());
        if (!matchOpt.isPresent()) {
            return append(doc);
        }
        YamlNode match = matchOpt.get();
        
        if (match.isRoot()) {
            return new Document(m_content.getChildLinesWithIndent(0));
        }
        if (!match.isMappingNode()) {
            throw new IllegalStateException("replacement of scalar nodes not supported, line: " + match.getStartLine());
        }

        List<String> lines = new ArrayList<>();
        lines.addAll(doc.lines().subList(0, match.getStartLine() + 1));
        lines.addAll(m_content.getChildLinesWithIndent(match.getChildIndent()));
        lines.addAll(doc.lines().subList(match.getEndLine() + 1, doc.lines().size()));
        
        return new Document(lines);
    }
    
    
    private Document append(Document doc) {
        YamlIndex docIndex = YamlIndexer.index(doc);
        
        List<String> segments = getPath().segments();
        for (int i = 0; i < segments.size(); i++) {
            YamlNode parent = docIndex.find(YamlPath.from(segments.subList(0, i))).get();
            
            YamlPath prefixPath = YamlPath.from(segments.subList(0, i + 1));
            if (!docIndex.find(prefixPath).isPresent()) {
                int childIndent = parent.getChildIndent();
                if (childIndent == 0 && !parent.isRoot()) {
                    childIndent = parent.getIndent() + 2;
                }
                
                StringBuffer buf = new StringBuffer();
                for (int j = 0; j < childIndent; j++) {
                    buf.append(" ");
                }
                buf.append(segments.get(i)).append(":");
                
                List<String> lines = new ArrayList<>();
                lines.addAll(doc.lines().subList(0, parent.getEndLine() + 1));
                lines.add(buf.toString());
                if (parent.getEndLine() + 1 <= doc.lines().size()) {
                    lines.addAll(doc.lines().subList(parent.getEndLine() + 1, doc.lines().size()));
                }
                
                doc = new Document(lines);
                docIndex = YamlIndexer.index(doc);
            }
        }
        
        YamlNode match = docIndex.find(getPath()).get();
        if (!match.isMappingNode()) {
            throw new IllegalStateException("not a mapping node at line: " + match.getStartLine());
        }
        List<String> lines = new ArrayList<>(doc.lines());
        int childIndent = match.getChildIndent();
        if (childIndent == 0 && !match.isRoot()) {
            childIndent = match.getIndent() + 2;
        }
        lines.addAll(match.getEndLine() + 1, m_content.getChildLinesWithIndent(childIndent));
        return new Document(lines);
    }
    
    
    private Document remove(Document doc) {
        YamlIndex docIndex = YamlIndexer.index(doc);
        
        Optional<YamlNode> matchOpt = docIndex.find(getPath());
        if (!matchOpt.isPresent()) {
            return doc;
        }
        YamlNode match = matchOpt.get();

        List<String> lines = new ArrayList<>();
        lines.addAll(doc.lines().subList(0, match.getStartLine()));
        lines.addAll(doc.lines().subList(match.getEndLine() + 1, doc.lines().size()));
        
        return new Document(lines);
    }
    
    
    private final YamlNode m_content;
    private final YamlPath m_path;
    private final PatchOperation m_operation;
}
