package org.apiblender;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class YamlNode {

    public YamlNode(Document doc) {
        m_parent = null;
        m_document = Objects.requireNonNull(doc, "doc must not be null");
        m_name = "";
        m_indent = -1;
        m_startLine = 0;
        m_endLine = doc.lines().size() - 1;
        m_isMappingNode = true;
    }
    
    
    public YamlNode(YamlNode parent, Document doc, String name, int indent, int startLine) {
        m_document = Objects.requireNonNull(doc, "doc must not be null");
        m_name = Objects.requireNonNull(name, "name must not be null");
        m_indent = indent;
        m_startLine = startLine;
        
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name must not be emtpy string");
        }
        if (m_startLine < 0) {
            throw new IllegalArgumentException("startLine must be >= 0, was " + m_startLine);
        }
        
        m_parent = Objects.requireNonNull(parent, "parent must not be null");
        
        if (m_indent < 0) {
            throw new IllegalArgumentException("indent must be >= 0, was " + m_indent);
        }
        if (m_indent <= m_parent.m_indent) {
            throw new IllegalArgumentException("indent must be > parent indent, was " + m_indent + " and  " + 
                    parent.m_indent);
        }
        
        m_parent.addChild(this);
    }
    
    
    public YamlNode getChild(String segmentName) {
        return m_children.get(segmentName);
    }
    
    
    public Collection<YamlNode> getChildren() {
        return Collections.unmodifiableCollection(m_children.values());
    }
    
    
    public List<String> lines() {
        return m_document.lines().subList(m_startLine, m_endLine + 1);
    }
    
    
    public String[] linesArray() {
        return lines().toArray(new String[0]);
    }
    
    
    public List<String> getChildLinesWithIndent(int newIndent) {
        List<String> lines = new ArrayList<>(m_document.lines().subList(m_startLine + 1, m_endLine + 1));
        
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            
            if (!line.trim().isEmpty()) {
            	StringBuilder buf = new StringBuilder();
                for (int j = 0; j < newIndent; j++) {
                    buf.append(" ");
                }
                
            	buf.append(line.substring(getChildIndent(), line.length()));
            	
            	lines.set(i, buf.toString());
            }
            else {
            	lines.set(i, "");
            }
        }
        
        return lines;
    }
    
    
    public int getIndent() {
        return m_indent;
    }
    
    
    public int getChildIndent() {
        return m_childIndent;
    }
    
    
    public int getStartLine() {
        return m_startLine;
    }
    
    
    public int getEndLine() {
        return m_endLine;
    }
    
    
    public boolean isMappingNode() {
        return m_isMappingNode;
    }
    
    
    public void dump(PrintWriter writer) {
        if (m_parent != null) {
            for (int i = 0; i < m_indent; i++) {
                writer.print(" ");
            }
            writer.println(m_name + " [" + m_startLine + "," + m_endLine + "]");
        }
        
        for (YamlNode child : m_children.values()) {
            child.dump(writer);
        }
        
        writer.flush();
    }
    
    
    public void setIsMappingNode(boolean isMappingNode) {
        m_isMappingNode = isMappingNode;
    }
    
    
    public boolean isRoot() {
        return m_parent == null;
    }
    
    
    void setChildIndent(int childIndent) {
        m_childIndent = childIndent;
    }
    
    
    void setEndLine(int endLine) {
        m_endLine = endLine;
        
        if (m_endLine < 0) {
            throw new IllegalArgumentException("endLine must be >= 0, was " + m_endLine);
        }
        if (m_endLine < m_startLine) {
            throw new IllegalArgumentException("startLine must be <= endline, was " + m_startLine + " and " + 
                    m_endLine);
        }
        if (m_endLine > m_document.lines().size()) {
            throw new IllegalArgumentException("endLine must be <= document length, was " + m_endLine + " and " +
                    m_document.lines().size());
        }
    }
    
    
    private void addChild(YamlNode child) {
        m_children.put(child.m_name, child);
    }
    
    
    private final int m_indent;
    private final int m_startLine;
    // FIXME: should be final (but makes recursion much more complex)
    private int m_endLine;
    private int m_childIndent = 0;
    private boolean m_isMappingNode = false;
    private final String m_name;
    private final Document m_document;
    private final YamlNode m_parent;
    
    private final Map<String,YamlNode> m_children = new HashMap<>();
}
