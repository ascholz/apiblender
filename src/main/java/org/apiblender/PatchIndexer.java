package org.apiblender;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatchIndexer {
    private static final Pattern opPattern = Pattern.compile("\\s*op:\\s+(\\w+)\\s*");
    
    private static final Pattern pathPattern = Pattern.compile("\\s*path:\\s+/(.*)\\s*");
    
    public static PatchIndex index(Document doc) {
        YamlIndex index = YamlIndexer.index(doc);
        
        Collection<Patch> patches = new ArrayList<>();
        
        
        for (YamlNode patchNode : index.root().getChildren()) {
            PatchOperation operation = null;
            YamlPath path = null;
            YamlNode content = null;;
            
            
            YamlNode opNode = patchNode.getChild("op");
            if (opNode == null) {
                throw new IllegalStateException("missing field: op");
            }
            Matcher opMatcher = opPattern.matcher(opNode.lines().get(0));
            if (!opMatcher.matches()) {
                throw new IllegalStateException("invalid op in line: " + opNode.getStartLine());
            }
            switch (opMatcher.group(1)) {
            case "append":
                operation = PatchOperation.APPEND;
                break;
            case "replace":
                operation = PatchOperation.REPLACE;
                break;
            case "remove":
                operation = PatchOperation.REMOVE;
                break;
            default:
                throw new IllegalStateException("unknown op " + opMatcher.group(1) + " in line: " + opNode.getStartLine());
            }
            
            
            YamlNode pathNode = patchNode.getChild("path");
            if (pathNode == null) {
                throw new IllegalStateException("missing field: path");
            }
            Matcher pathMatcher = pathPattern.matcher(pathNode.lines().get(0));
            if (!pathMatcher.matches()) {
                throw new IllegalStateException("invalid path in line: " + pathNode.getStartLine());
            }
            path = YamlPath.parse(pathMatcher.group(1));
            
            
            YamlNode contentNode = patchNode.getChild("content");
            if (contentNode == null) {
                throw new IllegalStateException("missing field: content");
            }
            content = contentNode;
            
            
            patches.add(new Patch(operation, path, content));
        }
        
        return new PatchIndex(patches);
    }
}
